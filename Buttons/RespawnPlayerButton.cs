using Godot;
using System;

public class RenderButton : Button
{
    MapGenerator MapScript;
    public override void _Ready()
    {
        MapScript = (MapGenerator)GetNode("/root/Main/TileMap");
    }
    public override void _Pressed()
    {
        MapScript.Render();
    }

}
