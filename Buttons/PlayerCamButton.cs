using Godot;
using System;

public class PlayerCamButton : Button
{
    public Camera2D PlayerCam;
    public Button MapCamButton;

    // Called when the node enters the scene tree for the first time.
    public override void _Pressed()
    {
        PlayerCam = GetNode<Camera2D>("//root/Main/Player/PlayerCam");
        MapCamButton = GetNode<Button>("//root/Main/Player/MapCamButton");
        PlayerCam.Current = true;
        MapCamButton.Show();

    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
}
