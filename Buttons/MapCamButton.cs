using Godot;
using System;

public class MapCamButton : Button
{
    public Camera2D MapCam;

    // Called when the node enters the scene tree for the first time.
    public override void _Pressed()
    {
        MapCam = GetNode<Camera2D>("//root/Main/ButtonLayer/MapCam");
        MapCam.Current = true;
        this.Hide();
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
}
