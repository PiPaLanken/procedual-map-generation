using Godot;
using System;
using System.Collections.Generic;

public class MapGenerator : Godot.TileMap
{
    [Export]
    int MapSizeX = 1920 * 2; //1920
    [Export]
    int MapSizeY = 1080 * 2; //1080
    [Export]
    int TileSize = 32; //32
    public int AmountX;
    public int AmountY;
    [Export]
    int neighbors = 5; //3
    [Export]
    int VoidChance = 2; //4
    [Export]
    int ExpandRate = 3; //1
    [Export]
    int ConnectSize = 1; //1
    public int ExpandTiles = 1; //1
    enum Tiles
    {
        GROUND, CORNERFILLER, BLACK, SIDEWALL, TRIANGLETOP, TOP, TRIANGLE, VOID, YELLOW,
        TOPBOT, YELLOW2, TOPBOTGRASSAROUND, TOPBOTGRASSRIGHT, CORNERFILLERMIRROR, CORNERFILLERTRIBBLE, FILLER, NEWTOPBOT, SIDEWALLNOGRASS
    }
    public Random random;
    public TileMap tileMap;
    public UtilChance Util;
    public Camera2D playerCam;
    public KinematicBody2D player;

    public override void _Ready()
    {
        random = new Random();
        Util = new UtilChance();
        Clear();
        Fill_Map();
        Random_Void();
        for (int a = 0; a < ExpandRate; a++) { Expand_Void(); Yellow_To_Void(); }
        Connect_Caves();
        Expand_Connection();
        Yellow_To_Void();
        Create_Yellow_Border();
        Render();
        playerCam = GetNode<Camera2D>("//root/Main/Player/PlayerCam");
        playerCam.LimitBottom = MapSizeY + TileSize / 2;
        playerCam.LimitRight = MapSizeX;
        PlayerSpawnPoint();
    }
    public void Fill_Map()
    {
        AmountX = Calculator(MapSizeX);
        AmountY = Calculator(MapSizeY);
        for (int b = 0; b < AmountY; b++)
        {
            for (int a = 0; a < AmountX; a++)
            {
                this.SetCell(a, b, (int)Tiles.BLACK);
            }
        }
    }
    public void Random_Void()
    {
        for (int b = 1; b < AmountY - 1; b++)
        {
            for (int a = 1; a < AmountX - 1; a++)
            {
                if (Util.chance(VoidChance))
                    this.SetCell(a, b, (int)Tiles.VOID);
            }
        }
    }
    public void Expand_Void()
    {
        int xSize = AmountX - 1;
        int ySize = AmountY - 1;
        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                if (this.GetCell(x, y) == (int)Tiles.VOID)
                {
                    if (x > 1) this.SetCell(x - ExpandTiles, y, (int)Tiles.YELLOW);
                    if (x < xSize - 1) this.SetCell(x + ExpandTiles, y, (int)Tiles.YELLOW);
                    if (y > 1) this.SetCell(x, y - ExpandTiles, (int)Tiles.YELLOW);
                    if (y < ySize - 1) this.SetCell(x, y + ExpandTiles, (int)Tiles.YELLOW);
                }
            }
        }
    }
    public void Yellow_To_Void()
    {
        for (int d = 1; d < AmountY - 1; d++)
        {
            for (int c = 1; c < AmountX - 1; c++)
            {
                if (this.GetCell(c, d) == (int)Tiles.YELLOW)
                {
                    this.SetCell(c, d, (int)Tiles.VOID);
                }
            }
        }
    }
    public void Connect_Caves()
    {
        int xSize = AmountX - 1;
        int ySize = AmountY - 1;
        int Count = 0; ;
        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                if (this.GetCell(x, y) == (int)Tiles.VOID)
                {
                    if (this.GetCell(x - 1, y) == (int)Tiles.BLACK && this.GetCell(x, y + 1) == (int)Tiles.BLACK && this.GetCell(x, y - 1) == (int)Tiles.BLACK)
                    {
                        for (Count = 0; Count < neighbors; Count++)
                        {
                            if (this.GetCell(x - 1 - Count, y) == (int)Tiles.VOID)
                            {
                                for (int c = 0; c < Count; c++)
                                {
                                    this.SetCell(x - 1 - c, y, (int)Tiles.YELLOW);
                                }
                            }
                        }
                        Count = 0;
                    }
                    if (this.GetCell(x + 1, y) == (int)Tiles.BLACK && this.GetCell(x, y + 1) == (int)Tiles.BLACK && this.GetCell(x, y - 1) == (int)Tiles.BLACK)
                    {
                        for (Count = 0; Count < neighbors; Count++)
                        {
                            if (this.GetCell(x + 1 + Count, y) == (int)Tiles.VOID)
                            {
                                for (int c = 0; c < Count; c++)
                                {
                                    this.SetCell(x + 1 + c, y, (int)Tiles.YELLOW);
                                }
                            }
                        }
                        Count = 0;
                    }
                }
            }
        }

    }
    public void Expand_Connection()
    {
        int xSize = AmountX - 1;
        int ySize = AmountY - 1;
        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                if (this.GetCell(x, y) == (int)Tiles.YELLOW)
                {
                    if (x > 1 && x < xSize && y > 1 && y < ySize - 1)
                    {
                        for (int c = 1; c <= ConnectSize; c++)
                        {
                            if (this.GetCell(x - 1, y) == (int)Tiles.YELLOW && this.GetCell(x + 1, y) != (int)Tiles.YELLOW)
                            {
                                this.SetCell(x + 1, y - c, (int)Tiles.VOID);
                                this.SetCell(x + 1, y + c, (int)Tiles.VOID);
                            }
                            if (this.GetCell(x + 1, y) == (int)Tiles.YELLOW && this.GetCell(x - 1, y) != (int)Tiles.YELLOW)
                            {
                                this.SetCell(x - 1, y - c, (int)Tiles.VOID);
                                this.SetCell(x - 1, y + c, (int)Tiles.VOID);
                            }
                            if (this.GetCell(x + 1, y) != (int)Tiles.YELLOW && this.GetCell(x - 1, y) != (int)Tiles.YELLOW)
                            {
                                this.SetCell(x - 1, y - c, (int)Tiles.VOID);
                                this.SetCell(x - 1, y + c, (int)Tiles.VOID);
                                this.SetCell(x + 1, y - c, (int)Tiles.VOID);
                                this.SetCell(x + 1, y + c, (int)Tiles.VOID);
                            }
                            this.SetCell(x, y - c, (int)Tiles.VOID);
                            this.SetCell(x, y + c, (int)Tiles.VOID);

                        }
                    }
                }
            }

        }
    }
    public void Create_Yellow_Border()
    {
        for (int y = 0; y < AmountY; y++)
        {
            this.SetCell(0, y, (int)Tiles.YELLOW);
            this.SetCell(AmountX - 1, y, (int)Tiles.YELLOW);
        }
        for (int x = 0; x < AmountX; x++)
        {
            this.SetCell(x, 0, (int)Tiles.YELLOW);
            this.SetCell(x, AmountY - 1, (int)Tiles.YELLOW);
        }
        for (int y = 0; y < AmountY; y++)
        {
            for (int x = 0; x < AmountX; x++)
            {
                if (this.GetCell(x, y) == (int)Tiles.YELLOW) this.SetCell(x, y, (int)Tiles.BLACK);
            }
        }
        for (int y = 0; y < AmountY + 1; y++)
        {
            this.SetCell(-1, y - 1, (int)Tiles.YELLOW);
            this.SetCell(AmountX, y - 1, (int)Tiles.YELLOW);
        }
        for (int x = 0; x < AmountX + 2; x++)
        {
            this.SetCell(x - 1, -1, (int)Tiles.YELLOW);
            this.SetCell(x - 1, AmountY, (int)Tiles.YELLOW);
        }

    }
    public void Render()
    {
        for (int y = 0; y < AmountY; y++)
        {
            for (int x = 0; x < AmountX; x++)
            {
                if (this.GetCell(x, y) == (int)Tiles.BLACK)
                {
                    RenderSwitch(x, y);
                }
            }
        }
    }
    public void RenderSwitch(int x, int y)
    {
        bool flipx = true;
        bool flipy = true;

        double sum = 0;
        for (int b = -1; b <= 1; b++)
        {
            for (int a = -1; a <= 1; a++)
            {
                if (this.GetCell(a + x, b + y) != (int)Tiles.VOID)
                {
                    int cellValue = (a + 1) + (b + 1) * 3;
                    sum += Math.Pow(2, cellValue);
                }
            }
        }
        if (this.GetCell(x, y) != (int)Tiles.YELLOW)
        {
            switch (sum)
            {
                //511-Summe an Voids
                //Ground
                case 504: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 508: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 505: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 509: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 440: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 248: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 327: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 258: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 464: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 47: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 208: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 184: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 92: this.SetCell(x, y, (int)Tiles.GROUND); break;
                case 144: this.SetCell(x, y, (int)Tiles.GROUND); break;
                //Sidewall ->
                case 219: this.SetCell(x, y, (int)Tiles.SIDEWALL, flipx); break;
                case 475: this.SetCell(x, y, (int)Tiles.SIDEWALL, flipx); break;
                case 223: this.SetCell(x, y, (int)Tiles.SIDEWALL, flipx); break;
                case 236: this.SetCell(x, y, (int)Tiles.SIDEWALL, flipx); break;
                case 275: this.SetCell(x, y, (int)Tiles.SIDEWALL, flipx); break;
                case 479: this.SetCell(x, y, (int)Tiles.SIDEWALL, flipx); break;
                //Sidewall <-
                case 503: this.SetCell(x, y, (int)Tiles.SIDEWALL); break;
                case 502: this.SetCell(x, y, (int)Tiles.SIDEWALL); break;
                case 439: this.SetCell(x, y, (int)Tiles.SIDEWALL); break;
                case 438: this.SetCell(x, y, (int)Tiles.SIDEWALL); break;
                case 22: this.SetCell(x, y, (int)Tiles.SIDEWALL); break;
                case 86: this.SetCell(x, y, (int)Tiles.SIDEWALL); break;
                case 498: this.SetCell(x, y, (int)Tiles.SIDEWALL); break;
                //Triangle ->
                case 217: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 473: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 409: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 153: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 472: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 216: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 408: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 152: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 303: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 35: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 34: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 430: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 167: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 413: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 477: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 221: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 212: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                case 222: this.SetCell(x, y, (int)Tiles.TRIANGLE, flipx); break;
                //Triangle <-
                case 180: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 436: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 244: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 432: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 176: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 240: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 496: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 500: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 372: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 305: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 309: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 93: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 10: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 400: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 368: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 401: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 501: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 245: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 437: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                case 435: this.SetCell(x, y, (int)Tiles.TRIANGLE); break;
                //TriangleTOP ->
                case 27: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 31: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 91: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 95: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 94: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 90: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 30: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 26: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 59: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 287: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 351: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 347: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 382: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 415: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 159: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                case 155: this.SetCell(x, y, (int)Tiles.TRIANGLETOP); break;
                //TriangleTOP <-
                case 311: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 310: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 306: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 307: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 50: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 54: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 55: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 51: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 62: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 119: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 137: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 134: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 375: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                case 246: this.SetCell(x, y, (int)Tiles.TRIANGLETOP, flipx); break;
                //507 = TopRight // 510 = TopLeft // 447 = BotLeft // 255 = BotRight
                case 507: this.SetCell(x, y, (int)Tiles.CORNERFILLER, flipx); break;
                case 510: this.SetCell(x, y, (int)Tiles.CORNERFILLER); break;
                case 447: this.SetCell(x, y, (int)Tiles.CORNERFILLER, !flipx, flipy); break;
                case 255: this.SetCell(x, y, (int)Tiles.CORNERFILLER, flipx, flipy); break;
                //Top
                case 383: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 111: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 319: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 63: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 18: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 23: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 388: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 449: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 384: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 127: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 58: this.SetCell(x, y, (int)Tiles.TOP); break;
                case 378: this.SetCell(x, y, (int)Tiles.TOP); break;
                //TopBot
                case 482: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 455: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 19: this.SetCell(x, y, (int)Tiles.NEWTOPBOT); break;
                case 24: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSRIGHT); break;
                case 48: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSRIGHT, flipx); break;
                case 56: this.SetCell(x, y, (int)Tiles.NEWTOPBOT); break;
                case 377: this.SetCell(x, y, (int)Tiles.NEWTOPBOT); break;
                case 304: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSRIGHT, flipx); break;
                case 88: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSRIGHT); break;
                case 25: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 52: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSRIGHT, flipx); break;
                case 17: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 272: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSAROUND); break;
                case 273: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 20: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSAROUND); break;
                case 80: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSAROUND); break;
                case 84: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 16: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSAROUND); break;
                case 312: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 120: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 308: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 89: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSRIGHT); break;
                case 121: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 316: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 376: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 166: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 483: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 175: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 458: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 61: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 29: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 49: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 345: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 60: this.SetCell(x, y, (int)Tiles.TOPBOTGRASSRIGHT); break;
                case 28: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 57: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 381: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 125: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 317: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 281: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 53: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 112: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 116: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 280: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 21: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                case 336: this.SetCell(x, y, (int)Tiles.TOPBOT); break;
                //SidewallNoGrass ->
                case 251: this.SetCell(x, y, (int)Tiles.SIDEWALLNOGRASS, flipx); break;
                //FillerMirror
                case 315: this.SetCell(x, y, (int)Tiles.CORNERFILLERMIRROR, flipx); break;
                case 254: this.SetCell(x, y, (int)Tiles.CORNERFILLERMIRROR); break;
                case 443: this.SetCell(x, y, (int)Tiles.CORNERFILLERMIRROR, flipx); break;
                case 478: this.SetCell(x, y, (int)Tiles.CORNERFILLERMIRROR); break;
                //CornerFillerTriple
                case 218: this.SetCell(x, y, (int)Tiles.CORNERFILLERTRIBBLE, flipx, flipy); break;
                case 379: this.SetCell(x, y, (int)Tiles.CORNERFILLERTRIBBLE, !flipx, flipy); break;
                case 444: this.SetCell(x, y, (int)Tiles.CORNERFILLERTRIBBLE, !flipx, flipy); break;

            }

        }
    }
    ////////////////////////////////////////////
    public void PlayerSpawnPoint()
    {
        player = GetNode<KinematicBody2D>("//root/Main/Player");
        bool IsntVoid = true;
        while (IsntVoid)
        {
            for (int y = 0; y < AmountY; y++)
            {
                for (int x = 0; x < AmountX; x++)
                {
                    if (this.GetCell(x, y - 1) == (int)Tiles.VOID && this.GetCell(x + 1, y) == (int)Tiles.VOID && this.GetCell(x, y + 1) == (int)Tiles.VOID && this.GetCell(x, y) == (int)Tiles.VOID)
                    {
                        int newX = CalculatorBack(x);
                        int newY = CalculatorBack(y);
                        player.Position = new Vector2(newX, newY);
                        GD.Print(x + " X  Y " + y);
                        IsntVoid = false;
                        return;
                    }
                }
            }
        }
    }
    public void PlayerDig(int input, Player player, int DestroyRange)
    {
        int x = PlayerPosCalc(this.player.Position.x);
        int y = PlayerPosCalc(this.player.Position.y);
        int xRange = 0;
        int yRange = 0;
        int SpecialX = 0;
        int SpecialY = 0;
        for (xRange = 0; xRange < DestroyRange; xRange++)
        {
            for (yRange = 0; yRange < DestroyRange; yRange++)
            {
                if (input == 1)
                {
                    if (this.GetCell(x, y - yRange) != (int)Tiles.YELLOW)
                    {
                        this.SetCell(x, y - yRange, (int)Tiles.VOID);
                        SpecialY = -yRange;
                    }
                }
                if (input == 2)
                {
                    if (this.GetCell(x - xRange, y) != (int)Tiles.YELLOW)
                    {
                        this.SetCell(x - xRange, y, (int)Tiles.VOID);
                        SpecialX = -xRange;
                    }
                }
                if (input == 3)
                {
                    if (this.GetCell(x, y + yRange) != (int)Tiles.YELLOW)
                    {
                        this.SetCell(x, y + yRange, (int)Tiles.VOID);
                        SpecialY = yRange;
                    }
                }
                if (input == 4)
                {

                    if (this.GetCell(x + xRange, y) != (int)Tiles.YELLOW)
                    {
                        this.SetCell(x + xRange, y, (int)Tiles.VOID);
                        SpecialX = xRange;
                    }

                }
            }
        }
        for (int a = -2; a < 3; a++)
        {
            for (int b = -2; b < 3; b++)
            {
                if (this.GetCell(a + x, b + y) != (int)Tiles.VOID)
                    RenderSwitch(x + a, y + b);
            }
        }
    }
    //////////////////////////////////////
    public int Calculator(int WindowSize)
    {
        float b = WindowSize / TileSize;
        int Output = (int)Math.Round(b);
        return Output;
    }
    public int PlayerPosCalc(float playerPos)
    {
        float b = playerPos / TileSize;
        GD.Print(b + " b");
        int Output = Convert.ToInt32(Math.Floor(b));
        GD.Print(Output + " Output");
        return Output;
    }
    public int CalculatorBack(int a)
    {
        int A = a * TileSize;
        return A;
    }

}
