using Godot;
using System;

public class Player : KinematicBody2D
{
    [Export]
    public int Gravity = 2500;
    [Export]
    public int Speed = 250;
    [Export]
    public int JumpSpeed = -900;
    public int DestroyRange = 2;
    Vector2 velocity = new Vector2();
    public Player player;
    MapGenerator MapScript;
    public override void _Ready()
    {
        MapScript = (MapGenerator)GetNode("/root/Main/TileMap");
    }
    public void GetInput()
    {
        velocity.x = 0;
        var right = Input.IsActionPressed("ui_right");
        var left = Input.IsActionPressed("ui_left");
        var jump = Input.IsActionPressed("ui_up");
        var attack = Input.IsActionPressed("ui_control");

        var DDown = Input.IsActionPressed("ui_digDown");
        var DUp = Input.IsActionPressed("ui_digUp");
        var DLeft = Input.IsActionPressed("ui_digLeft");
        var DRight = Input.IsActionPressed("ui_digRight");

        var animatedSprite = GetNode<AnimatedSprite>("PlayerSprite");
        if (IsOnFloor() && jump)
        {
            velocity.y = JumpSpeed;
        }
        if (!IsOnFloor()) animatedSprite.Animation = "Jump";
        if (right)
        {
            velocity.x += Speed;
            if (IsOnFloor())
                animatedSprite.Animation = "Walk";
            animatedSprite.FlipH = true;
        }
        if (left)
        {
            velocity.x -= Speed;
            if (IsOnFloor())
                animatedSprite.Animation = "Walk";
            animatedSprite.FlipH = false; ;
        }
        if (!left && !right && IsOnFloor()) animatedSprite.Animation = "Stand";

        if (DUp)
        {
            int w = 1;
            MapScript.PlayerDig(w, this.player, DestroyRange);
        }
        if (DLeft)
        {
            int a = 2;
            MapScript.PlayerDig(a, this.player, DestroyRange);
        }
        if (DDown)
        {
            int s = 3;
            MapScript.PlayerDig(s, this.player, DestroyRange);
        }
        if (DRight)
        {
            int d = 4;
            MapScript.PlayerDig(d, this.player, DestroyRange);
        }
    }
    public override void _PhysicsProcess(float delta)
    {
        velocity.y += Gravity * delta;
        GetInput();
        velocity = MoveAndSlide(velocity, new Vector2(0, -1));
    }
}
